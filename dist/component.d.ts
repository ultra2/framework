export declare class ComponentServer<P, S> {
    socket: any;
    emit(message: string, data: any, component?: string): void;
    log(message: string): void;
    send(command: string, data: any): void;
}
