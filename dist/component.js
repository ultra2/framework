"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ComponentServer {
    emit(message, data, component) {
        this.socket.emit(message, data);
    }
    log(message) {
        this.emit("log:write", message);
    }
    send(command, data) {
        if (!process.send) {
            console.log("no engine, cant send: ", command, data);
        }
        process.send({ command: command, data: data });
    }
}
exports.ComponentServer = ComponentServer;
