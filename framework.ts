import * as express from 'express'

export function run(a: string) {

  var app = express()

  app.use(express.static('node_modules/framework222-client/dist/client'))

  app.get("/ok", function (req, res) {
    res.send("framework: ok")
    res.end()
  })

  //WEB SERVER
  var http = require('http')
  //var https = require('https')

  var server = http.createServer(app)
  //var server = https.createServer(options, app).listen(443)

  //WEB SOCKET
  var ioserver = require('socket.io')(server);
  ioserver.on('connection', function (socket) {
    console.log("helloworld: new client socket connected")

    socket.use((params, next) => {
      var message = params[0]
      var splittedMessage = message.split(':')
      var component = splittedMessage[0]
      var method = splittedMessage[1]
      var data = params[1]

      var componentModule = require('./' + component + '.server')
      var componentInstance = new componentModule.default()
      componentInstance["socket"] = socket

      componentInstance[method](data)

      return next()
    })

    socket.on('disconnect', function () { });
  })

  var host = process.env.IP || "127.0.0.1"
  var port = parseInt(process.env.PORT) || 8080

  server.listen(port, host, function () {
    console.log('framework started on %s:%d ...', host, port);
  })

  console.log("framework run", a)
}
