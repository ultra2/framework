

export class ComponentServer<P, S> {
        
    public socket: any

    public emit(message: string, data: any, component?: string){
        //if (component) message = component + ":" + message
        //this.emitfn(message, data)
        this.socket.emit(message, data)
    }

    public log(message: string){
        this.emit("log:write", message)
    }

    public send(command: string, data: any){
        if (!process.send){
            console.log("no engine, cant send: ", command, data)
        }
        process.send({ command: command, data: data })
    }
}
